# New CTS failures in 1.3.7.0.
dEQP-VK.api.version_check.unavailable_entry_points,Fail

# Test bugs https://gitlab.khronos.org/Tracker/vk-gl-cts/-/issues/5074
dEQP-VK.pipeline.fast_linked_library.misc.unused_shader_stages,Crash
dEQP-VK.pipeline.fast_linked_library.misc.unused_shader_stages_include_geom,Crash
dEQP-VK.pipeline.fast_linked_library.misc.unused_shader_stages_include_tess,Crash
dEQP-VK.pipeline.fast_linked_library.misc.unused_shader_stages_include_tess_include_geom,Crash
dEQP-VK.pipeline.pipeline_library.misc.unused_shader_stages,Crash
dEQP-VK.pipeline.pipeline_library.misc.unused_shader_stages_include_geom,Crash
dEQP-VK.pipeline.pipeline_library.misc.unused_shader_stages_include_tess,Crash
dEQP-VK.pipeline.pipeline_library.misc.unused_shader_stages_include_tess_include_geom,Crash

# New CTS failures in 1.3.8.0.
dEQP-VK.api.copy_and_blit.sparse.image_to_image.simple_tests.partial_image_npot_diff_format_clear,Fail
dEQP-VK.api.copy_and_blit.sparse.image_to_image.simple_tests.partial_image_npot_diff_format_noclear,Fail
dEQP-VK.api.copy_and_blit.sparse.image_to_image.simple_tests.whole_image_diff_format,Fail
dEQP-VK.api.get_device_proc_addr.non_enabled,Fail
